SHELL	:= /bin/sh
CC		:= gcc

CFLAGS	:= -Wall -Wextra -Werror

.PHONY: run clean

raycast: raycast.c
	@$(CC) raycast.c -o raycast -lGL -lGLU -lglut -lm

run: raycast
	@./raycast

clean:
	@rm -f raycast
