# 2D / 3D Raycasting game engine

A simple raycasting game engine based on 3DSage's tutorial.

<div align="center" style="text-align:center">
	<img src="screenshots/screen.png" width="75%" />
</div>

## Introduction

This is a simple raycasting game engine based on the first part of
[this tutorial](https://www.youtube.com/watch?v=gYRrGTC7GtA). There is also a
[second part](https://www.youtube.com/watch?v=PC1RaETIx3Y).

You can move around by using the `WASD` keys and you can exit the game using the
`Q` key.

## Getting started

In order to be able to play the game, you need to compile it from source.

### Setting up the environment

Make sure to install the necessary packages by executing the following command:

```bash
# on Debian-based distros
sudo apt-get install build-essential libgl1-mesa-dev

# on Arch-based distros
sudo pacman -S build-essential libgl
```

### Building and running

To build the game you can issue this command explicitly:

```bash
gcc raycast.c -o raycast -lGL -lGLU -lglut -lm
```

Then run it by typing in:

```bash
./raycast
```

Or simply use `make` to build and run.

## Useful links

[Part 1.](https://www.youtube.com/watch?v=gYRrGTC7GtA)  
[Part 2.](https://www.youtube.com/watch?v=PC1RaETIx3Y)  
[Wikipedia/raycasting](https://en.wikipedia.org/wiki/Ray_casting)  
[lodev.org/raycasting](https://lodev.org/cgtutor/raycasting.html#:~:text=Raycasting%20is%20a%20rendering%20technique,vertical%20line%20of%20the%20screen.)  
[CodingTrain](https://www.youtube.com/watch?v=vYgIKn7iDH8)

## Licensing

This project is licensed under the [MIT](./LICENSE) license.
