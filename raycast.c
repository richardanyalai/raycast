/* Raycasting game engine based on 3DSage's work
 * Modified / updated by Richard Anyalai
 */

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define WIDTH	985
#define HEIGHT	512 

#define PI		3.1415926535
#define P2		(PI / 2)
#define P3		(3 * PI / 2)
#define DR		0.0174533

bool up, down, left, right;
float px, py, pdx, pdy, pa, lineH_prev = 0, lineO_prev = 0;
int mapX = 8, mapY = 8, mapS = 64;

uint8_t map[] =
{
	1, 1, 1, 1, 1, 1, 1, 1,
	1, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 1, 0, 0, 3, 0, 1,
	1, 0, 1, 1, 0, 0, 0, 1,
	1, 0, 1, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 2, 0, 0, 1,
	1, 0, 0, 0, 0, 1, 0, 1,
	1, 1, 1, 1, 1, 1, 1, 1
};

float degToRad(float a)
{
	return a * M_PI / 180.0;
}

float FixAng(float a)
{
	if (a > 359)
		a -= 360;

	if (a < 0)
		a += 360;

	return a;
}

float dist(float ax, float ay, float bx, float by, float ang)
{
	return
		sqrt(
			(bx - ax) * (bx - ax) +
			(by - ay) * (by - ay)
		);
}

void drawMap2D()
{
	int x, y, xo, yo;

	for (y = 0; y < mapY; ++y)
		for (x = 0; x < mapX; ++x)
		{
			if (map[y * mapX + x] == 3)
				glColor3f(0, 1, 0.5);
			else if (map[y * mapX + x] == 2)
				glColor3f(0, 0.5, 1);
			else if (map[y * mapX + x] == 1)
				glColor3f(1, 0.4, 0.3);
			else
				glColor3f(0, 0, 0);

			xo = x * mapS;
			yo = y * mapS;

			glBegin(GL_QUADS);
			glVertex2i(xo + 1,			yo + 1			);
			glVertex2i(xo + 1,			yo + mapS - 1	);
			glVertex2i(xo + mapS - 1,	yo + mapS - 1	);
			glVertex2i(xo + mapS - 1,	yo + 1			);
			glEnd();
		}
}

void goIfCan(float newX, float newY)
{
	int i_x = (int) (newX / mapS),
		i_y = (int) (newY / mapS);

	if (map[i_x + mapY * i_y] == 0)
	{
		px = newX;
		py = newY;
	}
	else if (map[(int) (px / mapS) + mapY * i_y] == 0)
		py = newY;
	else if (map[i_x + mapY * (int) (py / mapS)] == 0)
		px = newX;
}

void drawPlayer()
{
	glColor3f(1, 1, 0);
	glPointSize(8);
	glBegin(GL_POINTS);
	glVertex2i(px, py);
	glEnd();

	glLineWidth(3);
	glBegin(GL_LINES);
	glVertex2i(px, py);
	glVertex2i(px + pdx * 5, py + pdy * 5);
	glEnd();
}

void drawFloorAndCeiling()
{
	int floor_top	= mapY * mapS / 3,
		left		= mapX * mapS - 16,
		right		= 16 * (mapS - 1),
		top			= 0,
		bottom		= top + mapS - 1;

	glColor3f(0, 0.4, 1); // ceiling

	glBegin(GL_QUADS);
	glVertex2i(left + 18,	top + 1		); // top left
	glVertex2i(left + 18,	floor_top	); // bottom left
	glVertex2i(right,		floor_top	); // bottom right
	glVertex2i(right,		top			); // top right
	glEnd();

	float color = 0.0;

	for (int row = floor_top; row < HEIGHT; ++row)
	{
		glColor3f(color, color, 0);
		glLineWidth(3);

		glBegin(GL_QUADS);
		glVertex2i(left + 18,	row +  1); // top left
		glVertex2i(left + 18,	row + 11); // bottom left
		glVertex2i(right,		row + 11); // bottom right
		glVertex2i(right,		row +  1); // top right
		glEnd();

		color += 0.005;
	}
}

void drawRays()
{
	int r, mx, my, mp, dof, mv, mh;
	float rx, ry, ra, xo, yo, disT;

	ra = pa - DR * 30;

	if (ra < 0)
		ra += 2 * PI;

	if (ra > 2 * PI)
		ra -= 2 * PI;

	for (r = 0; r < 60; ++r)
	{
		/* Checking horizontal lines */

		dof = 0;

		float distH = 1000000,
			hx = px,
			hy = py,
			aTan = -1 / tan(ra);

		if (ra > PI)
		{
			ry = (((int) py >> 6) << 6) - 0.0001;
			rx = (py - ry) * aTan + px;
			yo = -64;
			xo = -yo * aTan;
		}

		if (ra < PI)
		{
			ry = (((int) py >> 6) << 6) + 64;
			rx = (py - ry) * aTan + px;
			yo = 64;
			xo = -yo * aTan;
		}

		if (ra == 0 || ra == PI)
		{
			rx = px;
			ry = py;
			dof = 8;
		}

		while (dof < 8)
		{
			mx = (int) rx >> 6;
			my = (int) ry >> 6;
			mp = my * mapX + mx;

			if (mp > 0 && mp < mapX * mapY && map[mp] > 0)
			{
				hx = rx;
				hy = ry;
				distH = dist(px, py, hx, hy, ra);
				dof = 8;
				mh = map[mp];
			}
			else
			{
				rx += xo;
				ry += yo;
				dof += 1;
			}
		}

		/* Checking vertical lines */

		dof = 0;

		float distV = 1000000,
			vx = px,
			vy = py,
			nTan = -tan(ra);

		if (ra > P2 && ra < P3)
		{
			rx = (((int) px >> 6) << 6) - 0.0001;
			ry = (px - rx) * nTan + py;
			xo = -64;
			yo = -xo * nTan;
		}

		if (ra < P2 || ra > P3)
		{
			rx = (((int) px >> 6) << 6) + 64;
			ry = (px - rx) * nTan + py;
			xo = 64;
			yo = -xo * nTan;
		}

		if (ra == 0 || ra == PI)
		{
			rx = px;
			ry = py;
			dof = 8;
		}

		while (dof < 8)
		{
			mx = (int) rx >> 6;
			my = (int) ry >> 6;
			mp = my * mapX + mx;

			if (mp > 0 && mp < mapX * mapY && map[mp] > 0)
			{
				vx = rx;
				vy = ry;
				distV = dist(px, py, vx, vy, ra);
				dof = 8;
				mv = map[mp];
			}
			else
			{
				rx += xo;
				ry += yo;
				dof += 1;
			}
		}

		if (distV < distH)
		{
			rx = vx;
			ry = vy;
			disT = distV;

			float clr = 1.2 - disT / 450;

			if (mv == 2)
				glColor3f(0, 0, clr);
			else if (mv == 3)
				glColor3f(0, clr, 0);
			else
				glColor3f(clr, 0, 0);
		}

		if (distH < distV)
		{
			rx = hx;
			ry = hy;
			disT = distH;

			float clr = 1 - disT / 450;

			if (mh == 2)
				glColor3f(0, 0, clr);
			else if (mh == 3)
				glColor3f(0, clr, 0);
			else
				glColor3f(clr, 0, 0);
		}

		glLineWidth(3);
		glBegin(GL_LINES);
		glVertex2i(px, py);
		glVertex2i(rx, ry);
		glEnd();

		/* Draw 3D walls */

		float ca = pa - ra;

		if (ca < 0)
			ca += 2 * PI;

		if (ca > 2 * PI)
			ca -= 2 * PI;

		disT *= cos(ca);

		float lineH = (mapS * 320) / disT;
		float lineO = 160 - lineH / 2;

		lineH_prev = lineH;
		lineO_prev = lineO;

		glLineWidth(8);
		glBegin(GL_QUADS);
		glVertex2i(r * 8 + 514, lineO_prev);				// upper left
		glVertex2i(r * 8 + 514, lineH_prev + lineO_prev);	// bottom left
		glVertex2i(r * 8 + 522, lineH + lineO);				// bottom right
		glVertex2i(r * 8 + 522, lineO);						// upper right
		glEnd();

		lineH_prev = lineH;
		lineO_prev = lineO;

		ra += DR;

		if (ra < 0)
			ra += 2 * PI;

		if (ra > 2 * PI)
			ra -= 2 * PI;
	}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	drawMap2D();
	drawFloorAndCeiling();
	drawRays();
	drawPlayer();
	glutSwapBuffers();
}

void buttonsDown(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 'w': up = true;	break;
		case 's': down = true;	break;
		case 'a': left = true; break;
		case 'd': right = true; break;
		case 'q': exit(0);		break;
	}
}

void buttonsUp(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 'w': up = false; break;
		case 's': down = false; break;
		case 'a': left = false; break;
		case 'd': right = false; break;
	}
}

void init()
{
	glClearColor(0.3, 0.3, 0.3, 0);
	gluOrtho2D(0, WIDTH, HEIGHT, 0);

	px = 300;
	py = 300;
	pdx = cos(pa) * 5;
	pdy = sin(pa) * 5;

	up = down = left = right = false;
}

void movePlayer()
{
	if (up)
		goIfCan(px + pdx, py + pdy);

	if (down)
		goIfCan(px - pdx, py - pdy);

	if (left)
	{
		pa -= 0.1;

		if (pa < 0)
			pa += 2 * PI;

		pdx = cos(pa) * 5;
		pdy = sin(pa) * 5;
	}

	if (right)
	{
		pa += 0.1;

		if (pa < 0)
			pa -= 2 * PI;

		pdx = cos(pa) * 5;
		pdy = sin(pa) * 5;
	}

	glutPostRedisplay();

	usleep(30000);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutCreateWindow("RayCasting");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(buttonsDown);
	glutKeyboardUpFunc(buttonsUp);
	glutIdleFunc(movePlayer);
	glutMainLoop();
}
